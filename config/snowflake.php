<?php

return [
    "format" => "d-m-Y | H:i:s",
    "timezone" => "Europe/Berlin"
];

?>
