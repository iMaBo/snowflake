## Description

Discord Snowflake Laravel Package


## Requirement

1. PHP >= 7.0
2. **[Composer](https://getcomposer.org/)**

## Installation

```shell
$ composer require mabo/snowflake
```

## Useage

1. Add SnowflakeServiceProvider to your config/app.php

```php
MaBo\Snowflake\SnowflakeServiceProvider::class
```

2. Publish the provider

```php
php artisan vendor:publish --provider="MaBo\Snowflake\SnowflakeServiceProvider" --tag="config"
```

3. Goto config/snowflake.php and change the settings

```php
<?php

return [
    "format" => "d-m-Y | H:i:s",
    "timezone" => "Europe/Berlin" // List of supported timezones https://www.php.net/manual/en/timezones.php
];

?>
```

4. add snowflake to your project

```php
use MaBo\Snowflake\Snowflake;

Snowflake::resolve($discordID);

// Example: Snowflake::resolve("129749996458344449")
// returns 25-12-2015 | 02:00:10
```

## License

MIT
