<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita59542284ff99c3c01c7c925268e9a06
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'MaBo\\Snowflake\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'MaBo\\Snowflake\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita59542284ff99c3c01c7c925268e9a06::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita59542284ff99c3c01c7c925268e9a06::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInita59542284ff99c3c01c7c925268e9a06::$classMap;

        }, null, ClassLoader::class);
    }
}
