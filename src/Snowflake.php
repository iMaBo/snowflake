<?php
    namespace MaBo\Snowflake;

    use DateTime;
    use DateTimeZone;

    class Snowflake
    {
        public static function resolve($discordID)
        {
             $timestamp = round((($discordID >> 22) + 1420070400000)/1000);

             $dt = new DateTime("@$timestamp");
             $dt->setTimezone(new DateTimeZone(config('snowflake.timezone')));

            return $dt->format(config('snowflake.format'));
        }
    }

?>
