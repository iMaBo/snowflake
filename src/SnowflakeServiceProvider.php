<?php

namespace MaBo\Snowflake;

use Illuminate\Support\ServiceProvider;

class SnowflakeServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->mergeConfigFrom(__DIR__.'/../config/snowflake.php', 'snowflake');
  }

  public function boot()
  {
    if ($this->app->runningInConsole()) {
        $this->publishes([
          __DIR__.'/../config/snowflake.php' => config_path('snowflake.php'),
        ], 'config');

      }
  }
}

?>
